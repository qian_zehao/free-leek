from flask import Flask
from flask import render_template

from src.read_data.read_json import read_json_data
from src.read_data.read_yml import read_yaml_data
from src.wx_send_msg import FreeLeekWeChat
from src.get_news_data import get_news

import time


def create_app(test_config=None):
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(SECRET_KEY='dev')

    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)

    @app.route('/test')
    def test():
        return render_template('TestPage.html')

    return app

# app = Flask(__name__, instance_relative_config=True)
# app.config.from_mapping(SECRET_KEY='dev')


# @app.route('/test')
# def test():
#     return render_template('TestPage.html')


while True:
    config_data = read_yaml_data('./config.yaml')
    
    news_data = get_news(config_data['tushare_tocken'])
    wx_interface_data = config_data['wx_interface']
    appid = wx_interface_data['appid']
    secret = wx_interface_data['secret']
    openid = wx_interface_data['openid']
    template_id = wx_interface_data['template_id']
    template_data = read_json_data('./message_template.json')
    
    title0 = news_data['title'][0]
    content0 = news_data['content'][0]
    title1 = news_data['title'][1]
    content1 = news_data['content'][1]
    # title2 = news_data['title'][2]
    # content2 = news_data['content'][2]
    # title3 = news_data['title'][3]
    # content3 = news_data['content'][3]
    # title4 = news_data['title'][4]
    # content4 = news_data['content'][4]


    template_data['title0']['value'] = title0
    template_data['content0']['value'] = content0
    template_data['title1']['value'] = title1
    template_data['content1']['value'] = content1
    # template_data['title2']['value'] = title2
    # template_data['content2']['value'] = content2
    # template_data['title3']['value'] = title3
    # template_data['content3']['value'] = content3
    # template_data['title4']['value'] = title4
    # template_data['content4']['value'] = content4

    msg = FreeLeekWeChat(appid, secret, openid, template_id, template_data)
    msg.post_data()
    time.sleep(config_data['time_interval'])
