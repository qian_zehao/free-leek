import json


def read_json_data(filename):
    try:
        file = open(filename, encoding='utf-8')
        data = json.loads(file.read())
        file.close()
        return data
    except Exception as error_msg:
        return error_msg
