# -*- coding: utf-8 -*-
import yaml


def read_yaml_data(filePath):
    try:
        file = open(filePath, encoding='utf-8')
        data = yaml.load(file.read(), Loader=yaml.FullLoader)
        file.close()
        return data
    except Exception as error_msg:
        return error_msg
