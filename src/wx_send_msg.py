# -*- coding: utf-8 -*-
import requests
import json


class FreeLeekWeChat():
    def __init__(self, appid, secret, openid, template_id, template_data):
        self.appid = appid
        self.secret = secret
        self.openid = openid
        self.template_id = template_id
        self.template_data = template_data

    def get_token(self):
        try:
            payload = {
                'grant_type': 'client_credential',
                'appid': self.appid,
                'secret': self.secret,
            }
            url = "https://api.weixin.qq.com/cgi-bin/token?"

            respone = requests.get(url, params=payload, timeout=50)
            access_token = respone.json().get("access_token")
            return access_token
        except Exception as error_msg:
            print(error_msg)

    def post_data(self):
        data = {
            "touser": self.openid,
            "template_id": self.template_id,
            "data": self.template_data
        }
        try:
            json_template = json.dumps(data)
            access_token = self.get_token()
            url = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token="+access_token
            respone = requests.post(url, data=json_template, timeout=50)
            errcode = respone.json().get("errcode")
            print("test--", respone.json())
            if(errcode == 0):
                print("The template message was sent successfully!")
            else:
                print("Template message delivery failed ^_^")
        except Exception as post_error_msg:
            print(post_error_msg)
