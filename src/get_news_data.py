import tushare as ts
from src.read_data.read_json import read_json_data
# init pro interface

def get_news(tushare_tocken):
    pro = ts.pro_api(tushare_tocken)

    # get data
    df = pro.jinse(**{
        "limit": 5,
    }, fields=[
        "title",
        "content",
        "type",
        "url",
        "datetime"
    ])

    return df
