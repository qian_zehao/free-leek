# -*- coding: utf-8 -*-

from src.read_data.read_json import read_json_data
from src.read_data.read_yml import read_yaml_data

from src.wx_send_msg import FreeLeekWeChat


if __name__ == "__main__":

    # appid = 'xxx'
    # secret = 'xxx'
    # openid = 'xxx'
    # template_id = 'xxx'

    # template_data = {
    #     "title": {
    #         "value": "Test titles",
    #         "color": "#173177"
    #     },
    #     "content": {
    #         "value": "Test content -- FreeLeek QianZeaho",
    #         "color": "#173177"
    #     },
    # }

    wx_interface = read_yaml_data('../config.yaml')
    wx_interface_data = wx_interface['wx_interface']
    appid = wx_interface_data['appid']
    secret = wx_interface_data['secret']
    openid = wx_interface_data['openid']
    template_id = wx_interface_data['template_id']
    template_data = read_json_data('../message_template.json')


    test = FreeLeekWeChat(appid, secret, openid, template_id, template_data)
    test.post_data()
