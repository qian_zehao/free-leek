from src.read_data.read_yml import read_yaml_data

ConfigFile = '../config.yaml'


# def read_yaml_data(filePath):
#     try:
#         file = open(filePath, encoding='utf-8')
#         data = yaml.load(file.read(), Loader=yaml.FullLoader)
#         file.close()
#         return data
#     except Exception as error_msg:
#         return error_msg


if __name__ == '__main__':
    data = read_yaml_data(ConfigFile)
    print(data)
