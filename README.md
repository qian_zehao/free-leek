# FreeLeek🍀 
## 0x00 Official websites
FreeLeek Home：[FreeLeek](http://198.74.121.61:803) <br />
Project Repository: https://gitee.com/qian_zehao/free-leek <br />
![](./example/pic/leekicon.jpeg)
## 0x01 Introduction
* FreeLeek is an open source server-end framework for transmiting financial data to app backend and WeChat platform by using Python Flask, Tushare Pro and Akshare.
* FreeLeek can automatically push finance news to WeChat official account subscribed by user.
* More users' function can be defined easily by editing python script such as quant trade algorithms, stock position management system.
![](./example/pic/example.jpeg)
## 0x02 User Instructions
Before developing and deploying this program, a lot configuration is required which are some account settings and environment configuration. If you didn't follow the instructions below, the program will not work properly. And if you have trouble, welcome contacting me through email: qianzehao123@gmail.com
### 2.1 Edit [config.yaml](config.yaml) file firstly
```json
wx_interface:
  appid: "<server appid>"
  secret: "<server secret>"
  openid: "<user wechat id>"
  template_id: "<user template_id>"

tushare_tocken: "<Your tushare token>"

time_interval: <set wx_msg_send interval>

```
#### 2.1.1 appid, secret and openid
WeChat Open Platform: https://mp.weixin.qq.com/debug/cgi-bin/sandboxinfo?action=showinfo&t=sandbox/index <br />
Step 1: Get appid and secret
![](./example/pic/appID.png)
Step 2: Scan QR Code and get openid
![](example/pic/openid.png)
Step 3: Edit [message_template.json](./message_template.json) file in both WeChat platform and this code document, after create test template, you will see the API as below.
```txt
template_title: # FreeLeek

template_content:👇👇👇
FreeLeek 财经新闻

{{title0.DATA}}
{{content0.DATA}} 

{{title1.DATA}}
{{content1.DATA}} 

-- By 自由韭菜基金会
```
![](example/pic/template_id.png)
#### 2.1.2 tushare_tocken
We get financial data from Tushare, before using you need to create an tushare pro account and duplicate token to the config.yaml file <br />
Tushare Register Port: https://tushare.pro/register?reg=559374 <br />
Tushare ProToken: https://tushare.pro/user/token <br />
#### 2.1.3 Set Software execute time interval
You can also set the interval of this application executing by editing the config.yaml file and the time unit is second.
### 2.2 Environment configuration
#### 2.2.1 In dev mode (conda environment)
In order to keep your OS environment clean, we strongly recommend that you can use [miniconda](https://docs.conda.io/en/latest/miniconda.html) to create Python virtual environment.
##### 2.2.1.1 Create Conda Environment
```bash
# Create a conda env named freeleek
$ conda create -n freeleek python=3.9
# activate freeleek with conda
$ conda activate freeleek
# install pip dependencies
$ pip install -r requirements.txt
```
##### 2.2.1.2 Run Backend Server (Linux and Mac OS)
```bash
# make sure conda env is freeleek
# cd /path/to/freeleek root directory
# set flask environment variables
$ export FLASK_DEBUG=True
$ export FLASK_APP=src
# Run!
$ flask run
```
##### 2.2.1.3 Run Backend Server (Windows OS)
```bash
# make sure conda env is freeleek
# cd /path/to/freeleek root directory
# set flask environment variables
$ set FLASK_DEBUG=True
$ set FLASK_APP=src
# Run!
$ flask run
```
#### 2.2.2 Install docker environment (Linux and Mac OS only)
If you want to deploy this project on your own server, VPS or even some ARM single board like RaspberryPi and Orange Pi, you have to install docker env in your computer. This may be the fastest method to install docker below.
```bash
$ export DOWNLOAD_URL="https://mirrors.tuna.tsinghua.edu.cn/docker-ce"
# if you use curl
$ curl -fsSL https://get.docker.com/ | sh
# or wget
$ wget -O- https://get.docker.com/ | sh
```
## 0x03 Deploy program in production environment with docker
Before deployment, you must make sure things as follows:
* [message_template.json](./message_template.json) and [config.yaml](config.yaml) file are ready
* Docker engine and docker-compose are installed.
* Edit [docker-compose.yaml](docker-compose.yaml) with service name, container name you like and local_port mapping.
```yaml
version: '3'

services:

  <backend_server_name>:
    container_name: freeleek_<name>
    build: .
    ports:
      - "<Any empty port>:5000"
    restart: always

```
After that, you can run it with your server by this command:
```bash
# build docker images
$ sudo docker-compose build
# start docker stack
$ sudo docker-compose up -d
```
And if you want to stop it by these commands:
```bash
# stop docker stack
sudo docker-compose down
# delete docker containers and images
$ sudo docker container prune
$ sudo docker image prune -a
```
## 0x04 More function development
